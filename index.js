console.log("Hello JSON");

/* JSON -Javascript Object Notation - it is a popular data format which application use to communicate with each other.

	JSON - aptly named after JS Objects looks like a Javascript Object with a {} and key-value pairs. However, the keys of a JSON object is surrounded by "".

	{
	"key1": "ValueA",
	"key2": 25000

	}
*/

let sampleJSON = `
	{
		"name": "Katniss Everdeen",
		"age": 20,
		"address": {
			"city": "Kansas City",
			"state": "Kansas"
		}

	}
`;
console.log(typeof sampleJSON);

// Are we able to turn a JSON into a JS Object?
// JSON.parse() - will return the given JSON as a JS object.
// JSON -> JS Object - JSON.parse()
let sampleObject1 = JSON.parse(sampleJSON);
console.log(sampleObject1);

// JSON.stringify() - will return a given JS object as a stringified JSON format string
// JS Object -> JSON - JSON.stringify()

let user1  = {
	username: "knight123",
	password: "1234",
	age: 25
}
let sampleStringified = JSON.stringify(user1);

console.log(sampleStringified);

// JSON Array
// Array of JSON in JSON format.

let sampleArr = `
	[
		{
			"email": "liester@gmail.com",
			"password": "1234qwee",
			"isAdmin": false
		},
		{
			"email": "john@gmail.com",
			"password": "pass222",
			"isAdmin": false
		}

	]
`;
console.log(sampleArr);	

// How can we delete the last item in the JSON array?

// First turn the JSON format array to a JS array
let parsedArr = JSON.parse(sampleArr);
console.log(parsedArr);

parsedArr.pop();
console.log(parsedArr);

// We can now turn the parsedArr back into JSON and update the sampleArr JSON array

sampleArr = JSON.stringify(parsedArr);
console.log(sampleArr);

// Database (JSON Format) => server (JSON Format to JS Object) => process (Task to do to the data) => turn the data back to JSON => client (Web page)


// Dos and Dont's of JSON Format

// Do: add double quotes to your keys
// Do: add colon after each key
// Dont's: don't add excessive commas
// Dont's: dont forget to close double quotes, curly brace
let sampleData = `
	{
		"email": "james22@gmail.com",
		"password": "1234james",
		"balance": 50000,
		"isAdmin": true
	}

`;
// When you parse JSON with an erratic format, it produces an error.
let parseData = JSON.parse(sampleData);
console.log(parseData);

// Sublime Text Tip: To help check the format of your JSON you can change the linting from JavaScript to JSON.